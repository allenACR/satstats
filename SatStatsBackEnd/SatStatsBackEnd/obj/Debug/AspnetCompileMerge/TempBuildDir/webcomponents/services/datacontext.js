(function () {

    angular.module('myapp').factory('datacontext',   ['$http', datacontext]);
    
    function datacontext() {
       
        //////////////////////////////////
        // METADATA SETUP
        var metadataStore = new breeze.MetadataStore();
        var manager = new breeze.EntityManager({
            serviceName: 'api/breeze/',
            metadataStore: metadataStore
        });

        var service = {
            login: login,
            register: register,
            fetchInfo: fetchInfo,
            fetchUsers: fetchUsers,
            saveChanges: saveChanges,
            createFacility: createFacility,
            createSurvey: createSurvey,
            createQuestion: createQuestion,
            fetchSureyWithId: fetchSureyWithId,
            fetchQuestionsWithId: fetchQuestionsWithId,
            fetchFacilityWidthId: fetchFacilityWidthId
        };

        return service;
        
        function fetchInfo(name) {

            var query = breeze.EntityQuery.from(name);
            return manager.executeQuery(query)
                .then(success,requestFailed);
            function success(data) {
                return data.results;
            }
        }
        //
        //////////////////////////////////


        //////////////////////////////////
        //  
        function login(username, password, callback) {


            var dataString = 'grant_type=password&username=' + username + '&password=' + password;
            $.ajax({
                type: 'POST',
                url: 'http://localhost:10611/Token',
                data: dataString,
                cache: false,
                contentType: 'application/x-www-form-urlencoded',
                processData: false,
                success: function (data) {

                    var token = data.access_token; 

                    fetchUserWithId(username, function (returnState, userData) {
                        

                        var siteUser = {
                            name: userData[0].FirstName + " " + userData[0].LastName,
                            role: userData[0].EmployeeType.toLowerCase(),
                            email: "",
                            location: "",
                            loginState: true,
                            token: token
                        };

                        // SAVE AS LOCAL VARIABLE
                        saveVar('siteUser', JSON.stringify(siteUser));

                        callback(true, data);

                    })



                },
                error: function (data, textStatus, errorThrown) {
                    callback(false, data);
                }
            });



        };
        //
        //////////////////////////////////

        //////////////////////////////////
        //  
        function register(dataString, callback) {


            var dataString = JSON.stringify(dataString);
  

            $.ajax({
                type: 'POST',
                url: 'http://localhost:10611/api/account/register',
                data: dataString,
                cache: false,
                contentType: 'application/json',
                processData: false,
                success: function (data) {
                    callback(true, data)
                },
                error: function (data, textStatus, errorThrown) {
                    callback(false, data);
                }
            });



        };
        //
        //////////////////////////////////

        //////////////////////////////////
        //  
        function fetchFacilityWidthId(id, callback) {
            var query = breeze.EntityQuery.from('facilities')
                                          .where('FacilityId', '==', id);

            return manager.executeQuery(query)
                .then(success, requestFailed);
            function success(data) {
                callback(true, data.results);
            }
        }
        //
        //////////////////////////////////

        //////////////////////////////////
        //  
        function fetchSureyWithId(id, callback) {
           
            var query = breeze.EntityQuery.from('surveys')
                                          .where('FacilityId', '==', id);

            return manager.executeQuery(query)
                .then(success, requestFailed);
            function success(data) {
                callback(true, data.results);
            }

        }
        //
        //////////////////////////////////


        //////////////////////////////////
        //  
        function fetchQuestionsWithId(id, callback) {

            var query = breeze.EntityQuery.from('questions')
                                          .where('SurveyId', '==', id);

            return manager.executeQuery(query)
                .then(success, requestFailed);
            function success(data) {                
                callback(true, data.results);
            }

        }
        //
        //////////////////////////////////

        //////////////////////////////////
        //  
        function fetchUserWithId(name, callback) {

            var query = breeze.EntityQuery.from('users')
                                          .where('UserName', '==', name);

            return manager.executeQuery(query)
                .then(success, requestFailed);
            function success(data) {
                callback(true, data.results);
            }

        }
        //
        //////////////////////////////////

        //////////////////////////////////
        //  
        function fetchUsers(callback) {
            

            var query = breeze.EntityQuery.from('users')
                                        
            return manager.executeQuery(query)
                .then(success, requestFailed);
            function success(data) {
                callback(true, data.results);
            }

        }
        //
        //////////////////////////////////




        //////////////////////////////////
        // CREATE NEW ENTRIES
        function createFacility(values) {
            manager.createEntity('Facility', values)            
        };
        //
        //////////////////////////////////


        //////////////////////////////////
        // CREATE NEW ENTRIES
        function createSurvey(values) {
            manager.createEntity('Survey', values)
        }
        //
        //////////////////////////////////

        //////////////////////////////////
        // CREATE NEW ENTRIES
        function createQuestion(values) {
            manager.createEntity('Question', values)
        }
        //
        //////////////////////////////////


        //////////////////////////////////
        // SAVE CHANGES
        function saveChanges(callback) {
              
            var token = loadVar("accessToken");   
            var ajaxAdapter = breeze.config.getAdapterInstance("ajax");
            /*
            ajaxAdapter.defaultSettings = {
                headers: {
                    "Authorization": "Bearer " + token
                }
            };
            */
            
          
            if (manager.hasChanges()) {
                manager.saveChanges()
                    .then(
                        function saveSucceeded(data) {
                            callback(true);
                        },
                        function saveFailed(error) {
                            callback(false);
                        }
                    );
            } else {                                
                callback(false);
            };
                    
        };
        //
        //////////////////////////////////

        //////////////////////////////////
        // REQUEST FAILED
        function requestFailed(error) {
            var msg = 'Error ' + error.message;
            logError(msg, error);
            //throw error;
        }
        //
        //////////////////////////////////
    }
})();