﻿(function () {

    var serviceId = 'conversionFactory';
    angular.module('myapp').factory(serviceId, [customfactory]);


    function customfactory() {



        var service = {
            convertToDataArray: convertToDataArray,
            convertMultiDataArray: convertMultiDataArray,
            convertToStringList: convertToStringList,
            compareAgainst: compareAgainst,
            compareAgainstSingle: compareAgainstSingle,
            checkForKey: checkForKey,
            checkForEnter: checkForEnter
        };
        return service;


        function convertToDataArray(array, catToName) {

            returnArray = new Array();
            for (i = 0; i < array.length; i++) {
                var name = eval("array[i]." + catToName);
                lockState = false;
                // EXCEPTION LOCKS
                //if (name == "superadmin") { lockState = true; }
                //if (name == "public" && rootSettings.currentRoute == 'home') { lockState = true; }

                returnArray.push({ id: i, text: name, locked: lockState });
            };
            return returnArray;

        };

        function convertMultiDataArray(array, catName1, catName2) {

            returnArray = new Array();
            for (i = 0; i < array.length; i++) {
                var name = eval("array[i]." + catName1) + " " + eval("array[i]." + catName2);
                lockState = false;
                // EXCEPTION LOCKS
                //if (name == "superadmin") { lockState = true; }
               // if (name == "public" && rootSettings.currentRoute == 'home') { lockState = true; }

                returnArray.push({ id: i, text: name, locked: lockState });
            };
            return returnArray;

        }


        function convertToStringList(array, catToName) {
            returnArray = "";
            for (i = 0; i < array.length; i++) {
                var name = eval("array[i]." + catToName);
                returnArray += name;
                if (i < array.length - 1) {
                    returnArray += ", ";
                }
            };
            return returnArray;


        }

        function compareAgainst(array, catToName, compareArray, compareName) {


            returnArray = "";
            counter = 0;
            for (i = 0; i < array.length; i++) {
                var name = eval("array[i]." + catToName);
                for (n = 0; n < compareArray.length; n++) {

                    var name2 = eval("compareArray[n]." + compareName);
                    if (name == name2) {
                        if (counter > 0) { returnArray += " ,"; };
                        returnArray += i;
                        counter++;
                    }
                }

            };
            return returnArray;
        }

        function compareAgainstSingle(array, catToName, compareName) {

            returnArray = "";
            counter = 0;
            for (i = 0; i < array.length; i++) {
                var name = eval("array[i]." + catToName);
                if (name == compareName) {
                    returnArray += i;
                }

            };
            return returnArray;
        }

        function checkForKey(keyId) {
            // 9 IS TAB
            if (keyId == 9) {
                return true;
            }
            else {
                return false; 
            }

        }

        function checkForEnter(keyId) {
            // 13 is enter
            if (keyId == 13) {
                return true;
            }
            else {
                return false;
            }
        }


    };


})();