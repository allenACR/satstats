(function () {
    'use strict';

    var app = angular.module('myapp');

    // Configure Toastr
    //toastr.options.timeOut = 4000;
    //toastr.options.positionClass = 'toast-bottom-right';
    
    var events = {
        controllerActivateSuccess: 'controller.activateSuccess',
        spinnerToggle: 'spinner.toggle'
    };

    var config = {
        appErrorPrefix: '[Error] ', //Configure the exceptionHandler decorator
        docTitle: 'Sat Stats: ',
        events: events,
        remoteServicePrefix: 'http://localhost:1094',
        version: '1.0.0'
    };

    app.value('config', config);
    
    app.run(['$location', function ($location) {
        if ($location.host().indexOf('localhost') === 0) {
            config.host = 'http://localhost:1094';
        } else {
           // config.remoteServicePrefix = 'http://puntodestino.gravityjack.com';
        }
    }]);

//    app.config(['$locationProvider',function ($locationProvider) {
//            $locationProvider.html5Mode(true);
//        }
//    ]);
    
    app.config(['$logProvider', function ($logProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
    }]);
    
    //Configure the common services via commonConfig
    //app.config(['commonConfigProvider', function (cfg) {
    //    cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
    //    cfg.config.spinnerToggleEvent = config.events.spinnerToggle;
    //}]);
})();