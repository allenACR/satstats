// GLOBAL FUNCTIONS

	
	//////////////////////
	//  truncateText
	 function truncateText( text, limit ){
		var myText = text.toString()
		len = myText.length
		if(len>limit)
		{
			return myText.substr(0,limit)+'...'
		}	
		else{
		
			return myText
		}
		
	}
	//		
	///////////////////////	
	
	
	//////////////////////
	//  
	function saveVar(varName, content, callback ) {
		
	 	globalStorage.set( varName, content )
	 	var msg = "(SAVE) " + varName + " = " + content + ""; 
	 	trace(msg, "success")
	 	
	 	if (callback != undefined){
	 		callback()	 		
	 	}
	}
	// saveVar("myName", "Callback", function(){	})  // CALLBACK 
	// saveVar("myName", "NoCallback")					// NO CALLBACK
	/////////////////////	
	
	//////////////////////
	//
	function loadVar(varName, callback) {						
	
		
	 	var data = globalStorage.get( varName ) 
		
	 	
	 	if (data == undefined){
	 		var msg = varName + " does not exist.";
	 		trace(msg, "error");
	 	}
	 	else{
	 		var msg = "(LOAD) " + varName + " = " + data;
	 		trace(msg, "info");
	 	}	

	 	if (callback != undefined){
	 		callback(data)	 		
	 	}
	 	else{
	 		return(data)
	 	}
	 	
	}
	// 	loadVar("myName", function(returnData){	})		// WITH CALLBACK
	//  loadVar("myName")								// NO CALLBACK
	/////////////////////		
	
	//////////////////////
	//  
	function deleteVar(varName, callback) {
		globalStorage.remove(varName);
	 	var msg = varName + " deleted.";
	 	trace(msg, "info");		
		
	 	if (callback != undefined){
	 		callback()	 		
	 	}			

	}
	// 	deleteVar("myName");
	/////////////////////	
	
	
	
	//////////////////////
	//  GET INFO ON STORAGE TYPE/SIZE  
	function storageInfo(){
		
		// build alert message
		var info = [
		  'Backend: ',
		  Persist.type || 'none',
		  ', ',
		  'Approximate Size Limit: ',
		  (Persist.size < 0) ? 'unknown' : Persist.size 
		].join('');
		
		// prompt user with information
		trace(info);		
		
	}
	
	//////////////////////	
	
	//////////////////////
	// TRACE MESSAGES	
	function trace(msg, type){
		
		
		if (type == undefined){ type = "trace"}
		
		if (rootSettings.debugMode){
			$.bootstrapGrowl(truncateText(msg, 150), {
			  ele: 'body', // which element to append to
			  type: type, // (null, 'info', 'error', 'success')
			  offset: {from: 'bottom', amount: 20}, // 'top', or 'bottom'
			  align: 'right', // ('left', 'right', or 'center')
			  width: 'auto', // (integer, or 'auto')
			  delay: 10000,
			  allow_dismiss: true,
			  stackup_spacing: 30 // spacing between consecutively stacked growls.
			});	
		}
			
	}
	//
	/////////////////////	



	//////////////////////
	// TRACE MESSAGES	
	function console(msg, type){
		
		if (type == undefined){ type = "trace"}
		

			$.bootstrapGrowl(truncateText(msg, 150), {
			  ele: 'body', // which element to append to
			  type: type, // (null, 'info', 'error', 'success')
			  offset: {from: 'bottom', amount: 20}, // 'top', or 'bottom'
			  align: 'right', // ('left', 'right', or 'center')
			  width: 'auto', // (integer, or 'auto')
			  delay: 1200,
			  allow_dismiss: false,
			  stackup_spacing: 30 // spacing between consecutively stacked growls.
			});	
	
	}
	//
	/////////////////////	
	

	/////////////////////
	//
	function validateStandards(checkString) {
	    // lower case and underscore allowed for first character
	    // all others must be lower case and numbers allowed
	    //checkString = checkString.replace(" ", "");  <!-- REENABLE TO AUTOMATICALLY ELIMINATE SPACES
	    if (/^[a-zA-Z _][a-zA-Z0-9 _]*$/.test(checkString) == false) {
	        return false;
	    } else {
	        return true;

	    }
	}

	function validateString(checkString){
        
	    if (/^[a-zA-Z0-9?$@#()'!,+\-=_:.&��*%\s]+$/.test(checkString) == false) {
	        return false;
	    } else {
	        return true;
	    };
	}
	//
	/////////////////////