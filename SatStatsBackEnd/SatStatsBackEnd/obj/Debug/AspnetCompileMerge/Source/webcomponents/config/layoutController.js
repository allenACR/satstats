	// MAIN CONTROLLER
	myApp.controller('indexController', function($scope, $location, $anchorScroll, datacontext, thecodefactory) {

	        $scope.isActive = true;
	        $scope.isLoggedIn = false;

	        /////////////////////////////////////
	        /////// MONIOR KEYPRESS
	        $('html').keydown(function (e) {
	            if (thecodefactory.checkForSequence(e.which)) {
	                $('#specialModal').modal('show');
	            }
	        });
	        //
	        ///////////////////////////////////// 

	        /////////////////////////////////////
	        /////// LOGOUT
	        $scope.logout = function() {
	                deleteVar('siteUser');
	                location.reload();
	            };
	        //
	        /////////////////////////////////////            

	        /////////////////////////////////////
        	/////// GRAB ALL DATA
	        function fetchPackage(name, callback) {
	            datacontext.fetchInfo( name )
                    .then(function (results) {                    
                        callback(true, results)
                    },
                        function (err) {
                            console.log("Error: getFacilities" + err.message);
                            callback(false)
                        });
	        };
	        //
	        /////////////////////////////////////

	        /////////////////////////////////////
	        // FETCH FACILITY DATA
	        $scope.refreshFacility = function() {
	            fetchPackage("facilities", function (returnState, returnData) {
	                $scope.facility = []; $scope.facility = returnData;
	                $scope.$broadcast('facilityLoaded');
	              
	            });
	        }
	        //
	        /////////////////////////////////////


	        /////////////////////////////////////
	        // FETCH FACILITY DATA
	        $scope.refreshUsers = function () {
	                fetchPackage("users", function (returnState, returnData) {
	                    $scope.users = []; $scope.users = returnData;
	                    $scope.$broadcast('usersLoaded');
	                });
	        }	        
	        //
	        /////////////////////////////////////

	        


	        /////////////////////////////////////
            // QUERY SURVEY BY ID
	        $scope.refreshSurveyById = function (id) {

	                datacontext.fetchSureyWithId(id, function (returnState, returnData) {
	                    if (returnState) {
	                        $scope.survey = []; $scope.survey = returnData;
	                        $scope.$broadcast('surveyLoaded');
	                    }

	                })

	        }
	        //
	        /////////////////////////////////////
	       
	        /////////////////////////////////////
	        // QUERY SURVEY BY ID
	        $scope.refreshQuestionsById = function (id) {
	           
	            datacontext.fetchQuestionsWithId(id, function (returnState, returnData) {
                    
	                    if (returnState) {	                       
	                        $scope.questions = []; $scope.questions = returnData;	                        
	                        $scope.$broadcast('questionsLoaded');
	                    }

	                })
	            }
	        //
	        /////////////////////////////////////


	        /////////////////////////////////////
	        // THINKING 
	        $scope.thinking = function(mode){
	            if(mode == 'show'){
	                $('#thinkingModal').modal('show');
	            }
	            if(mode == 'hide'){
	                $('#thinkingModal').modal('hide');
	            }
	        }
	        //
	        /////////////////////////////////////

	        
	        /////////////////////////////////////
            // STARTUP 
	        $scope.init = function(){
	            $scope.refreshFacility();
	        }
	        $scope.init();
	        //
	        /////////////////////////////////////

	      

          //
	    /////////////////////////////////////


	    //// CREATE 


        

		  /////////////////////////////////////
		  // SET VARIABLES

			if (loadVar('siteUser') == null) {
			 
                $scope.siteUser = {
                    name: 	"Guest",
                    role: 	"public",
                    email: 	"",
                    location: "",
                    imageURL: "http://dummyimage.com/250x250/000/fff",
                    loginState: false
                };
                saveVar('siteUser', JSON.stringify($scope.siteUser));
             
			}
		  //
		  /////////////////////////////////////


			$scope.toggleCollapse = function(){
				
	    		$scope.isActive = !$scope.isActive;
	    		$scope.applyToggle($scope.isActive);
				
	 	  	};
	 	  	
	 	  	$scope.applyToggle = function( state ){
	 	  		$scope.isActive = state;	 	  		
	 	  		$scope.$apply();
	 	  	};
	   		
	   		
	   	  ///////////////////////////////////
	   	  //  CHANGE BUTTON
	   	  	$scope.changeButton = function(type){
				$scope.onCustomize 	= 'media/navbar/TabBar_CustomizeBtn_Up.png';
				$scope.onMeasure 	= 'media/navbar/TabBar_MeasureBtn_Up.png';
				$scope.onReport 	= 'media/navbar/TabBar_ReportBtn_Up.png';
					
				if (type == "customize"){
					$scope.onCustomize 	= 'media/navbar/TabBar_CustomizeBtn_Dwn.png';
					$scope.statImage 	= "media/stats/Cust_PageTitle.png";
				}
				if (type == "measure"){
					$scope.onMeasure 	= 'media/navbar/TabBar_MeasureBtn_Dwn.png';
					$scope.statImage 	= "media/stats/Meas_PageTitle.png";
				}
				if (type == "reporting"){
					$scope.onReport 	= 'media/navbar/TabBar_ReportBtn_Dwn.png';
					$scope.statImage 	= "media/stats/report_PageTitle.png";
				}
				if (type == "admin"){
					$scope.onReport 	= 'media/navbar/TabBar_ReportBtn_Dwn.png';
					$scope.statImage 	= "media/stats/Admin_PageTitle.png";
				}				
				
				$scope.$apply();
			};
		  //	
		  ///////////////////////////////////   	  
	   
		  ///////////////////////////////////
		  // AFTER NEW VIEW IS LOADED
		  // THIS IS FIRED	    	
			$scope.$on('$stateChangeSuccess', function (event, toState) {
			
				$scope.changeLayout('standard');
				var currentRoute 	=  $location.path().substring(1);
				changeHash(currentRoute);	
				$scope.changeButton(currentRoute);
				
                
			});	
		  //
		  /////////////////////////////////////			
  
		  /////////////////////////////////////
		  // CHANGE LAYOUT
			$scope.changeLayout = function(type){
				// SELECT LAYOUT TYPE
				switch(type)
				{
					case "standard":
						$scope.layoutComponent_1 = "col-md-12 overflow-none";
						$scope.layoutComponent_2 = "col-md-12 overflow-none";
						$scope.layoutComponent_3 = "col-md-12 overflow-none";
						$scope.layoutComponent_4 = "col-md-12 overflow-none";	
						break;
																								
				}
		  };
		  //
		  /////////////////////////////////////
		  
		  
		  
		  
		  


            
	
					
				
	});
	
	myApp.run(['$q', 'use$q', function ($q, use$q) {
	    use$q($q);
	}]);



