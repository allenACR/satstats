	
(function () {
    myApp.controller('footerController', function($scope) {
	
        $scope.message = "We've raised the bar on personal care.  SatStats, co-founded by an Emergency Physician and a Customer Satisfaction Specialist, has given hospitals and clinics the power to offer point-of-care surveys to their patients in a simple to use handheld app.  As part of our all in one solution, we provide the iPad devices, survey app, statistical analysis and interpretation.  Our clients recieve the best data to manage their providers and nursing staff in real-time.  Contact us to learn more about how we can help your faculity.";
        $scope.emailSent = true;

        $scope.toggleBtn = true; 
        $scope.toggleBtnSrc; 
		
        $scope.isSuperAdmin = false;


        ////////////////////////////////
        // INIT
        $scope.init = function () {
		    
            loadVar('siteUser', function (data) {
              
                $scope.user = JSON.parse(data);
                if ($scope.user.role == 'superadmin') {
                    $scope.isSuperAdmin = true;
                }
            })
		 
        };
        //
        ///////////////////////////////
		
        ////////////////////////////////
        // SWITCH ICON		
        $scope.applyToggle(true);		
		
        $scope.switchIcon = function(){
		
            $scope.toggleCollapse();
            $scope.toggleBtn = !$scope.toggleBtn;
            checkBtnState();
					
        };
        //
        ///////////////////////////////

        ////////////////////////////////
        // CHECK BTN STATE
        function checkBtnState(){
            if ($scope.toggleBtn){
                $scope.toggleBtnSrc = "media/FooterBtn_Dwn.png";
				
            }
            else{
                $scope.toggleBtnSrc = "media/FooterBtn_Up.png";
            }
			
		
        }
        //
        ///////////////////////////////		

        ///////////////////////////////
        // CONTACT BTN		
        $scope.contactBtn = function(){
			

            var contactName 	= $scope.userName;
            var contactNum 		= $scope.userContactNum;
            var contactMsg 		= $scope.userContactMsg;
			

            $scope.emailSent = !$scope.emailSent;
            $scope.$apply();
					
			
			
            $.ajax({ url: 'php/phpFunctions.php',
                data: {action: 'sendEmail', name: contactName, contact: contactNum, content: contactMsg},	                 
                type: 'post',
                success: function(data) {  
                    callback(true, data)	
                },
                error: function(data){
                    alert("Server did not respond.  Please try again.");
                    callback(false, data)   
                }
            });				
			
				
        };
        //
        ///////////////////////////////
		
        checkBtnState();
    });


}())