	
(function () {
    myApp.controller('statsController', function ($scope) {


        $scope.hospitalTotal = "";
        $scope.departmentTotal = "";
        $scope.personalTotal = ""

        $scope.init = function () {
            $scope.$on('facilityLoaded', function (event) {
                $scope.hospitalTotal = $scope.facility.length;
            });

            $scope.$on('usersLoaded', function (event) {
                $scope.personalTotal = $scope.users.length;
            });

            $scope.refreshFacility();
            $scope.refreshUsers();
        }


    });
}());


