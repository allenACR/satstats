﻿(function () {

    var serviceId = 'entitymanager';
    angular.module('myapp').factory(serviceId, [entitymanager]);
	
	
	function entitymanager(){
	
		breeze.config.initializeAdapterInstance('modelLibrary', 'backingStore', true);

        var serviceName = 'api/breeze/';
        var metadataStore = createMetadataStore();

        var provider = {
            metadataStore: metadataStore,
            newManager: newManager
        };
        
        return provider;
        
        function createMetadataStore() {
            var store = new breeze.MetadataStore();
            return store;
        }

        function newManager() {
            var mgr = new breeze.EntityManager({
                serviceName: serviceName,
                metadataStore: metadataStore
            });
            return mgr;
        }	
	
	
	};
	
	
})();