	
(function () {
    myApp.controller('measureC2Controller', function ($scope) {

        $scope.measureCopy = 'Imagine handing patients our electronic survey during the discharge process.  The hands-on survey allows them the ability to quick and accurately rate their care in real-time.  Sampling the entire population of patients provides confidence & actionable data, improving overall patient satisfaction.  Our software provides administrators with immediate data on provider & nursing staff performance, so you can more effectively manage your resources.';


    });
}());

