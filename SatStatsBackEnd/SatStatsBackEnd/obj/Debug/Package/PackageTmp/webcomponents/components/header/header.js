	
(function () {
myApp.controller('headerController', function ($scope, datacontext) {
	
		$scope.message = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
		$scope.loginState = false; 
		$scope.loadingState = false; 

		$scope.openModal = function () {
		    $scope.loginFail = false;
		    $scope.$apply();
			$('#loginModal').modal('show');
			
		};
		
		$scope.forgotPassword = function(){
			alert("Forgot password.");
		};
		
		$scope.startLogin = function () {

		    $scope.loadingState = true;$scope.$apply();

		    setTimeout(function () {
		        datacontext.login($scope.username, $scope.password, function (returnState, data) {
		          


		            if (returnState) {
		                $('#loginModal').modal('hide');
		                setTimeout(function () {
		                    console("Login successful.");
		                    changeHash('#/customize');
		                })
		            }
		            else {
		                $scope.loadingState = false
		                $scope.loginFail = true;
		                $scope.$apply();
		            }
		        });
		    }, 500);

		};		
		

		$scope.init = function () {
		    var siteUser = JSON.parse(loadVar('siteUser'));
		    $scope.loginState = siteUser.loginState;
		}
		

		
		
	});
}());

