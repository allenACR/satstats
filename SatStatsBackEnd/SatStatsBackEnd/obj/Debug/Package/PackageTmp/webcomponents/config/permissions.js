	
	
	////////////////////////  UPDATE LOCATION
	function changeHash(newHash){	
		

		if( checkPermissions(newHash)){
			location.hash = newHash; // get the clicked link id
		}
		else{
			changeHash("#/home"); // get the clicked link id	
		}
		
	}
	////////////////////////	
	
	
	////////////////////////  CHANGE SCENES USING HASHTAGS	
	//
	function checkPermissions(requested){
		
		var siteUser = JSON.parse(loadVar('siteUser'));		// GET USER PERMISSION		
		requested = requested.substring(2);				// REMOVE HASHTAG AND BACKSLASH
		
	    // TYPES
        // --------------
	    // superadmin
	    // admin
	    // public
		

		switch (siteUser.role){
			case "superadmin":
				return true;
			break;			
			
			case "admin":
				return true;			// ACCESS TO ALL PAGES
			break;

		    case "doctor":
		        if (requested == 'customize') {
		            return false;
		        }
		        else {
		            return true;		// ACCESS TO ALL PAGES
		        }
		    break;

		    case "nurse":
		        if (requested == 'customize') {
		            return false;
		        }
		        else {
		            return true;		// ACCESS TO ALL PAGES
		        }
		    break;
				
			case "public":
				if (requested == 'home'){	
					return true;
				}
				else{
					return false;		// ACCESS TO ALL PAGES
				}		
			break;
		}
  		 
		return false; 					
		
	
	};
	//
	////////////////////////	
