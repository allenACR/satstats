
    
    
var myApp = angular.module('myapp', ["ui.router", "ngAnimate", "breeze.angular.q", "filters"]);
    myApp.config(function($stateProvider, $urlRouterProvider,  $locationProvider){
    
	// For any unmatched url, redirect to /state1
	$urlRouterProvider.otherwise("/home");
  	//
  	

  	
  	
  	//$scope.changeButton("customize");
    
    $stateProvider
        .state('index', {
            url: "/home",
            views: {
                "component_1": {
                    templateUrl: "webcomponents/components/header/header.html"
                },
                "component_2": {
                    templateUrl: "webcomponents/components/home-c1/home-c1.html"
                },
				"component_3": {
				    templateUrl: "webcomponents/components/home-c2/home-c2.html"
                },
				"component_4": {
				    templateUrl: "webcomponents/components/home-c3/home-c3.html"
                }, 
				"component_5": {
				    templateUrl: "webcomponents/components/home-c4/home-c4.html"
                },
				"component_6": {
				    templateUrl: "webcomponents/components/footer/footer.html"
                }                                   
            },
            
        })
        .state('customize', {
            url: "/customize",
            views: {
                "component_1": {
                    templateUrl: "webcomponents/components/header-user/header-user.html"
                },
                "component_2": {
                    templateUrl: "webcomponents/components/customize-c1/customize-c1.html"
                },
				"component_3": {
				    templateUrl: "webcomponents/components/stats/stats.html"
                },
				"component_4": {
				    templateUrl: "webcomponents/components/customize-c2/customize-c2.html"
                }, 
				"component_5": {
				    templateUrl: "webcomponents/components/empty/empty.html"
                },
				"component_6": {
				    templateUrl: "webcomponents/components/footer-alt/footer-alt.html"
                }  
            }
        })
        .state('measure', {
            url: "/measure",
            views: {
                "component_1": {
                    templateUrl: "webcomponents/components/header-user/header-user.html"
                },
                "component_2": {
                    templateUrl: "webcomponents/components/measure-c1/measure-c1.html"
                },
				"component_3": {
				    templateUrl: "webcomponents/components/stats/stats.html"
                },
				"component_4": {
				    templateUrl: "webcomponents/components/measure-c2/measure-c2.html"
                }, 
				"component_5": {
				    templateUrl: "webcomponents/components/empty/empty.html"
                },
				"component_6": {
				    templateUrl: "webcomponents/components/footer-alt/footer-alt.html"
                }  
            }
        })
        .state('reporting', {
            url: "/reporting",
            views: {
                "component_1": {
                    templateUrl: "webcomponents/components/header-user/header-user.html"
                },
                "component_2": {
                    templateUrl: "webcomponents/components/report-c1/report-c1.html"
                },
				"component_3": {
				    templateUrl: "webcomponents/components/stats/stats.html"
                },
				"component_4": {
				    templateUrl: "webcomponents/components/report-c2/report-c2.html"
                }, 
				"component_5": {
				    templateUrl: "webcomponents/components/empty/empty.html"
                },
				"component_6": {
				    templateUrl: "webcomponents/components/footer-alt/footer-alt.html"
                }  
            }
        })
        .state('admin', {
            url: "/admin",
            views: {
                "component_1": {
                    templateUrl: "webcomponents/components/header/header.html"
                },
                "component_2": {
                    templateUrl: "webcomponents/components/empty/empty.html"
                },
				"component_3": {
				    templateUrl: "webcomponents/components/stats/stats.html"
                },
				"component_4": {
				    templateUrl: "webcomponents/components/admin-c2/admin-c2.html"
                },
				"component_5": {
				    templateUrl: "webcomponents/components/empty/empty.html"
                },
				"component_6": {
				    templateUrl: "webcomponents/components/footer-alt/footer-alt.html"
                }                   
            }
        });          
              
        
        
    });



