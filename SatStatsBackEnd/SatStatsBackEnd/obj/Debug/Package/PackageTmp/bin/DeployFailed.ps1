$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
& (Join-Path "$scriptPath" SatStatsBackEnd.exe) -buildversion "$OctopusReleaseNumber" -rollbacktopreviousbuild true | Write-Host
