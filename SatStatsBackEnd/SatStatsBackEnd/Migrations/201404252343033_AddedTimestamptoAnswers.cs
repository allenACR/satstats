namespace SatStatsBackEnd.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTimestamptoAnswers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "Created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "Created");
        }
    }
}
