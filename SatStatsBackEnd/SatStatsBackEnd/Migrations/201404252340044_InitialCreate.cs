namespace SatStatsBackEnd.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
//            CreateTable(
//                "dbo.Answers",
//                c => new
//                    {
//                        AnswerId = c.Int(nullable: false, identity: true),
//                        QuestionId = c.Int(nullable: false),
//                        Value = c.Int(nullable: false),
//                        SurveyInstanceId = c.Int(nullable: false),
//                    })
//                .PrimaryKey(t => t.AnswerId)
//                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
//                .ForeignKey("dbo.SurveyInstances", t => t.SurveyInstanceId, cascadeDelete: true)
//                .Index(t => t.QuestionId)
//                .Index(t => t.SurveyInstanceId);
//            
//            CreateTable(
//                "dbo.Questions",
//                c => new
//                    {
//                        QuestionId = c.Int(nullable: false, identity: true),
//                        SurveyId = c.Int(),
//                        QuestionType = c.String(),
//                        QuestionContent = c.String(),
//                    })
//                .PrimaryKey(t => t.QuestionId)
//                .ForeignKey("dbo.Surveys", t => t.SurveyId)
//                .Index(t => t.SurveyId);
//            
//            CreateTable(
//                "dbo.Surveys",
//                c => new
//                    {
//                        SurveyId = c.Int(nullable: false, identity: true),
//                        FacilityId = c.Int(),
//                        SurveyName = c.String(),
//                    })
//                .PrimaryKey(t => t.SurveyId)
//                .ForeignKey("dbo.Facilities", t => t.FacilityId)
//                .Index(t => t.FacilityId);
//            
//            CreateTable(
//                "dbo.Facilities",
//                c => new
//                    {
//                        FacilityId = c.Int(nullable: false, identity: true),
//                        FacilityName = c.String(),
//                    })
//                .PrimaryKey(t => t.FacilityId);
//            
//            CreateTable(
//                "dbo.AspNetUsers",
//                c => new
//                    {
//                        Id = c.String(nullable: false, maxLength: 128),
//                        FirstName = c.String(),
//                        LastName = c.String(),
//                        EmployeeType = c.String(),
//                        FacilityId = c.Int(),
//                        Email = c.String(maxLength: 256),
//                        EmailConfirmed = c.Boolean(nullable: false),
//                        PasswordHash = c.String(),
//                        SecurityStamp = c.String(),
//                        PhoneNumber = c.String(),
//                        PhoneNumberConfirmed = c.Boolean(nullable: false),
//                        TwoFactorEnabled = c.Boolean(nullable: false),
//                        LockoutEndDateUtc = c.DateTime(),
//                        LockoutEnabled = c.Boolean(nullable: false),
//                        AccessFailedCount = c.Int(nullable: false),
//                        UserName = c.String(nullable: false, maxLength: 256),
//                    })
//                .PrimaryKey(t => t.Id)
//                .ForeignKey("dbo.Facilities", t => t.FacilityId)
//                .Index(t => t.FacilityId)
//                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
//            
//            CreateTable(
//                "dbo.AspNetUserClaims",
//                c => new
//                    {
//                        Id = c.Int(nullable: false, identity: true),
//                        UserId = c.String(nullable: false, maxLength: 128),
//                        ClaimType = c.String(),
//                        ClaimValue = c.String(),
//                    })
//                .PrimaryKey(t => t.Id)
//                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
//                .Index(t => t.UserId);
//            
//            CreateTable(
//                "dbo.AspNetUserLogins",
//                c => new
//                    {
//                        LoginProvider = c.String(nullable: false, maxLength: 128),
//                        ProviderKey = c.String(nullable: false, maxLength: 128),
//                        UserId = c.String(nullable: false, maxLength: 128),
//                    })
//                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
//                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
//                .Index(t => t.UserId);
//            
//            CreateTable(
//                "dbo.AspNetUserRoles",
//                c => new
//                    {
//                        UserId = c.String(nullable: false, maxLength: 128),
//                        RoleId = c.String(nullable: false, maxLength: 128),
//                    })
//                .PrimaryKey(t => new { t.UserId, t.RoleId })
//                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
//                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
//                .Index(t => t.UserId)
//                .Index(t => t.RoleId);
//            
//            CreateTable(
//                "dbo.SurveyInstances",
//                c => new
//                    {
//                        SurveyInstanceId = c.Int(nullable: false, identity: true),
//                        SurveyId = c.Int(nullable: false),
//                    })
//                .PrimaryKey(t => t.SurveyInstanceId)
//                .ForeignKey("dbo.Surveys", t => t.SurveyId, cascadeDelete: true)
//                .Index(t => t.SurveyId);
//            
//            CreateTable(
//                "dbo.AspNetRoles",
//                c => new
//                    {
//                        Id = c.String(nullable: false, maxLength: 128),
//                        Name = c.String(nullable: false, maxLength: 256),
//                    })
//                .PrimaryKey(t => t.Id)
//                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
//            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.SurveyInstances", "SurveyId", "dbo.Surveys");
            DropForeignKey("dbo.Answers", "SurveyInstanceId", "dbo.SurveyInstances");
            DropForeignKey("dbo.Questions", "SurveyId", "dbo.Surveys");
            DropForeignKey("dbo.Surveys", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.AspNetUsers", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.SurveyInstances", new[] { "SurveyId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "FacilityId" });
            DropIndex("dbo.Surveys", new[] { "FacilityId" });
            DropIndex("dbo.Questions", new[] { "SurveyId" });
            DropIndex("dbo.Answers", new[] { "SurveyInstanceId" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.SurveyInstances");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Facilities");
            DropTable("dbo.Surveys");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
        }
    }
}
