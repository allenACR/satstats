using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SatStatsBackEnd.Models;

namespace SatStatsBackEnd.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            // Add the role
            context.Roles.AddOrUpdate(r => r.Name, new IdentityRole(){Name = "SuperAdmin"});

            // Create the superadmin
            ApplicationUser user = null;
            const string username = "superadmin";
            const string password = "phone-joe.snow";
            
            user = userManager.FindByName(username);
            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = username,
                    FirstName = "Super",
                    LastName = "Admin",
                    EmployeeType = "SuperAdmin"
                };
                var result = userManager.Create(user, password);
                user = userManager.FindByName(username); // might not need this line
            }

            if (!userManager.IsInRole(user.Id, "SuperAdmin"))
            {
                userManager.AddToRole(user.Id, "SuperAdmin");
            }
        }
    }
}
