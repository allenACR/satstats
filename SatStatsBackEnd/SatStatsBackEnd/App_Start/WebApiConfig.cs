﻿using System.Web.Http;

namespace SatStatsBackEnd
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            // this is done individually on each controller
//            config.Routes.MapHttpRoute(
//                name: "BreezeApi",
//                routeTemplate: "api/breeze/{action}",
//                defaults: new { controller = "Breeze" }
//            );     
//
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
