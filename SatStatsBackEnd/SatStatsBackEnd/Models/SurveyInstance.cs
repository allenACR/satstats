using System.ComponentModel.DataAnnotations;

namespace SatStatsBackEnd.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SurveyInstance
    {
        public SurveyInstance()
        {
            this.Answers = new HashSet<Answer>();
        }

        public int SurveyInstanceId { get; set; }
        [Required]
        public int SurveyId { get; set; }
    
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual Survey Survey { get; set; }
    }
}
