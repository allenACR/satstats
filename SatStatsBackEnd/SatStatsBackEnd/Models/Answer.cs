using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SatStatsBackEnd.Models
{
    public class Answer
    {
        [Required]
        public int QuestionId { get; set; }
        [Required]
        public int Value { get; set; }
        [Required]
        public int SurveyInstanceId { get; set; }
        public int AnswerId { get; set; }
        public DateTime Created { get; set; }
    
        public virtual Question Question { get; set; }
        public virtual SurveyInstance SurveyInstance { get; set; }
    }
}
