//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SatStatsBackEnd.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Facility
    {
        public Facility()
        {
            this.AspNetUsers = new HashSet<ApplicationUser>();
            this.Surveys = new HashSet<Survey>();
        }
    
        public string FacilityName { get; set; }
        public int FacilityId { get; set; }

        public virtual ICollection<ApplicationUser> AspNetUsers { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
    }
}
