﻿using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Breeze.ContextProvider.EF6;
using Breeze.WebApi2;
using Breeze.ContextProvider;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using SatStatsBackEnd.Models;

namespace SatStatsBackEnd.Controllers
{

//    public class ApplicationMetadataDbContext : ApplicationDbContext
//    {
//        protected override void OnModelCreating(DbModelBuilder modelBuilder)
//        {
//            base.OnModelCreating(modelBuilder);
//            modelBuilder.Entity<IdentityUser>().Ignore(t => t.Claims);
//            modelBuilder.Entity<IdentityUser>().Ignore(t => t.Logins);
//            modelBuilder.Entity<IdentityUser>().Ignore(t => t.PasswordHash);
//            modelBuilder.Entity<IdentityUser>().Ignore(t => t.Roles);
//            modelBuilder.Entity<IdentityUser>().Ignore(t => t.SecurityStamp);
//        }
//    }

    
    [BreezeController]
    [RoutePrefix("api/breeze")]
    public class BreezeController : ApiController
    {
        readonly EFContextProvider<ApplicationDbContext> _db = new EFContextProvider<ApplicationDbContext>();
        
        [HttpGet]
        [Route("metadata")]
        public string Metadata()
        {
            var metaContextProvider = new EFContextProvider<ApplicationDbContext>();
            return metaContextProvider.Metadata();
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpPost]
        [Route("savechanges")]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            return _db.SaveChanges(saveBundle);
        }

        [HttpGet]
        [Route("facilities")]
        public IQueryable<Facility> Facilities()
        {
            if (ClaimsPrincipal.Current.IsInRole("Admin") || ClaimsPrincipal.Current.IsInRole("SuperAdmin"))
            {
                return _db.Context.Facilities;
            }

            var userName = ClaimsPrincipal.Current.Identity.Name;
            var facility = _db.Context.Facilities.FirstOrDefault(f => f.AspNetUsers.Any(u => u.UserName == userName));
            if (facility == null) return null;
            return _db.Context.Facilities.Where(f => f.FacilityId == facility.FacilityId);
        }

        [HttpGet]
        [Route("surveys")]
        public IQueryable<Survey> Survey()
        {
            if (ClaimsPrincipal.Current.IsInRole("Admin") || ClaimsPrincipal.Current.IsInRole("SuperAdmin"))
            {
                return _db.Context.Surveys;
            }
            
            var userName = ClaimsPrincipal.Current.Identity.Name;
            var facility = _db.Context.Facilities.FirstOrDefault(f => f.AspNetUsers.Any(u => u.UserName == userName));
            if (facility == null) return null;
            return _db.Context.Surveys.Where(f => f.FacilityId == facility.FacilityId);
        }

        [HttpGet]
        [Route("surveyinstances")]
        public IQueryable<SurveyInstance> SurveyInstance()
        {
            if (ClaimsPrincipal.Current.IsInRole("Admin") || ClaimsPrincipal.Current.IsInRole("SuperAdmin"))
            {
                return _db.Context.SurveyInstance;
            }

            var userName = ClaimsPrincipal.Current.Identity.Name;
            var facility = _db.Context.Facilities.FirstOrDefault(f => f.AspNetUsers.Any(u => u.UserName == userName));
            if (facility == null) return null;
            return _db.Context.SurveyInstance.Where(si => si.Survey.FacilityId == facility.FacilityId);
        }

        [HttpGet]
        [Route("questions")]
        public IQueryable<Question> Questions()
        {
            if (ClaimsPrincipal.Current.IsInRole("Admin") || ClaimsPrincipal.Current.IsInRole("SuperAdmin"))
            {
                return _db.Context.Questions;
            }

            var userName = ClaimsPrincipal.Current.Identity.Name;
            var facility = _db.Context.Facilities.FirstOrDefault(f => f.AspNetUsers.Any(u => u.UserName == userName));
            if (facility == null) return null;
            return _db.Context.Questions.Where(q => q.Survey.FacilityId == facility.FacilityId);
        }

        //[Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("users")]
        public IQueryable<ApplicationUser> Users()
        {
            return _db.Context.Users;
        }

        [Authorize(Roles = "Admin, SuperAdmin")]
        [HttpGet]
        [Route("answers")]
        public IQueryable<Answer> Answer()
        {
            return _db.Context.Answers;
        }
    }
}
