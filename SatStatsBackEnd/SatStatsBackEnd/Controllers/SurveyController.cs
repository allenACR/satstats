﻿using System.Linq;
using System.Web.Http;
using SatStatsBackEnd.Models;

namespace SatStatsBackEnd.Controllers
{

    [Authorize]
    [RoutePrefix("api/surveyinstances")]
    public class SurveyInstanceController : ApiController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        
        [HttpGet]
        [Route("surveyinstances")]
        public IQueryable<Survey> Survey()
        {
            return _db.Surveys;
        }

        [HttpGet]
        [Route("surveyinstances")]
        public IQueryable<SurveyInstance> SurveyInstance()
        {
            return _db.SurveyInstance;
        }

        [HttpGet]
        [Route("questions")]
        public IQueryable<Question> Questions()
        {
            return _db.Questions;
        }

        [HttpGet]
        [Route("users")]
        public IQueryable<ApplicationUser> Users()
        {
            return _db.Users; //http://www.asp.net/web-api/videos/getting-started/paging-and-querying
        }

        [HttpGet]
        [Route("answers")]
        public IQueryable<Answer> Answer()
        {
            return _db.Answers;
        }
    }
}
