﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SatStatsBackEnd.Models;

namespace SatStatsBackEnd.Controllers
{
    public class SurveyInstancesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/SurveyInstances
        public IQueryable<SurveyInstance> GetSurveyInstance()
        {
            return db.SurveyInstance;
        }

//        // GET: api/SurveyInstances/5
//        [ResponseType(typeof(SurveyInstance))]
//        public async Task<IHttpActionResult> GetSurveyInstance(int id)
//        {
//            SurveyInstance surveyInstance = await db.SurveyInstance.FindAsync(id);
//            if (surveyInstance == null)
//            {
//                return NotFound();
//            }
//
//            return Ok(surveyInstance);
//        }
//
//        // PUT: api/SurveyInstances/5
//        [ResponseType(typeof(void))]
//        public async Task<IHttpActionResult> PutSurveyInstance(int id, SurveyInstance surveyInstance)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            if (id != surveyInstance.SurveyInstanceId)
//            {
//                return BadRequest();
//            }
//
//            db.Entry(surveyInstance).State = EntityState.Modified;
//
//            try
//            {
//                await db.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!SurveyInstanceExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }
//
//            return StatusCode(HttpStatusCode.NoContent);
//        }

        // POST: api/SurveyInstances
        [ResponseType(typeof(SurveyInstance))]
        public async Task<IHttpActionResult> PostSurveyInstance(SurveyInstance surveyInstance)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SurveyInstance.Add(surveyInstance);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = surveyInstance.SurveyInstanceId }, surveyInstance);
        }

//        // DELETE: api/SurveyInstances/5
//        [ResponseType(typeof(SurveyInstance))]
//        public async Task<IHttpActionResult> DeleteSurveyInstance(int id)
//        {
//            SurveyInstance surveyInstance = await db.SurveyInstance.FindAsync(id);
//            if (surveyInstance == null)
//            {
//                return NotFound();
//            }
//
//            db.SurveyInstance.Remove(surveyInstance);
//            await db.SaveChangesAsync();
//
//            return Ok(surveyInstance);
//        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

//        private bool SurveyInstanceExists(int id)
//        {
//            return db.SurveyInstance.Count(e => e.SurveyInstanceId == id) > 0;
//        }
    }
}