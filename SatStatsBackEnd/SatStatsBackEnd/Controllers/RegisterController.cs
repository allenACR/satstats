﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SatStatsBackEnd.Models;

namespace SatStatsBackEnd.Controllers
{
    [Authorize]
    public class RegisterController : ApiController
    {

 /*         public RegisterController()
        {
        }

          public RegisterController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }*/

        // GET api/register
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/register/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/register
        public HttpResponseMessage Post(RegisterBindingModel model)
        {
            //UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(ApplicationDbContext());

            var user = new ApplicationUser { 
                UserName = model.Email,
                Email = model.Email,
                EmployeeType = model.EmployeeType, 
                FirstName = model.FirstName,
                LastName = model.LastName,
                FacilityId = model.FacilityId
            };



            //Try to add the new user to the database
            try
            {
                //TODO : Add user to DB

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT api/register/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/register/5
        public void Delete(int id)
        {
        }
    }
}
