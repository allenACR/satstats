	
(function () {
    myApp.controller('customC2Controller', function ($scope, datacontext, conversionFactory, $http) {

		$scope.customizeCopy = 'You have the ease and ability to customize our electronic survey directly for the staff and departmental needs.  This allows each specific hospital department needs.  This allows each specific hospital department to design and investigate unique concerns, and fully understand the interactino of every patient with your staff.';
		
		$scope.init = function () {  
            

		    $scope.setDefault = function () {                
		        $scope.surveyActive = false;
		        $scope.surveyReady = false;
		        $scope.facilitySelected = false;
		        $scope.questionActive = false;
		        $scope.facilityIsLoaded = false;
		        $scope.userIsLoaded = false;
		        $scope.singleUserLoaded = false; 
		        $scope.quesitonReady = false;
		        $scope.isCollapsed = true;
		        $scope.tabActive = 1;
		        $scope.questionType = "1";
		    }
		    $scope.setDefault();



		    $scope.questionTypeData = {
		        data: [ { id: 0, text: 'Doctor' },
                        { id: 1, text: 'Nurse' },
                        { id: 2, text: 'Facility' },
                        { id: 3, text: 'Other' }]
		    }

		    $scope.roleData = {
		        data: [ { id: 0, text: 'Admin' },
                        { id: 1, text: 'Doctor' },
                        { id: 2, text: 'Nurse' }]
		    }

		    $scope.passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,8}$/;

		    
		    // GET INFO 
		    //
		    $scope.refreshUsers();
		    $scope.refreshFacility();
            //
		    //////////
   
		    //////////////////////////////////
		    // TOGGLE FACILITY/PERSONNEL
		    $scope.toggleTab = function (type) {
		        $scope.tabActive = type
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // ADD NEW PERSONNEL
		    $scope.addNewPersonnel = function () {
		       
		        var passCheck = true;

		        var userEmail = $scope.newUserEmail;
		            if (userEmail == "" || userEmail == undefined) { passCheck = false };
		        var userName = $scope.newUserName;
		            if (userName == "" || userName == undefined) { passCheck = false };
		        var firstName = $scope.newFirstName;
		            if (firstName == "" || firstName == undefined) { passCheck = false };
		        var lastName = $scope.newLastName;
		            if (lastName == "" || lastName == undefined) { passCheck = false };
		        var password = $scope.newPassword;
		            if (password == "" || password == undefined) { passCheck = false };
		        var validate = $scope.newReenter;
		            if (validate == "" || validate == undefined) { passCheck = false };
		       
		        if ($('#select-personnelRole').val() != '') {
		            var role = $scope.roleData.data[$('#select-personnelRole').val()].text
		        }
		            if (role == "" || role == undefined) { passCheck = false }
		        if ($('#select-personnelFacility').val() != '') {
		            var facilityName = $scope.facility[$('#select-personnelFacility').val()].FacilityName
		            var facilityId = $scope.facility[$('#select-personnelFacility').val()].FacilityId
		        }
		            if (facilityName == "" || facilityName == undefined) { passCheck = false }
		            if (facilityId == "" || facilityId == undefined) { passCheck = false }
		          

		        if (password == validate) {
		            var dataString = {
		                email: userEmail,
		                password: password,
		                firstName: firstName,
		                lastName: lastName,
		                employeeType: role,
		                userName: userName,
		                facilityId:  facilityId
		            }
		            
		            
		            if (!passCheck) {
		                alert("All fields must be filled out.")
		            }
		            else {
		                $('#addBtn').attr("disabled", "enabled");

		                datacontext.register(dataString, function (returnState, data) {
		                    $('#addBtn').removeAttr("disabled");
		                    if (returnState) {		                       
		                        console("User has been added.");
		                    }
		                    else {
		                        alert("Username and/or email is already in use.  Please try again.");
		                    }
		                });
		            }
		        }
		        else {
		                alert("Passwords do not match.  Please try again.");
		        };
                
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // UPDATE PERSONNEL
		    $scope.updatePersonnel = function () {
		        var userName = $scope.editUserName; 
		        var firstName = $scope.editFirstName;
		        var lastName = $scope.editLastName;		       
		        var role = $scope.roleData.data[$('#select-editPersonnelRole').val()].text
		        if ($('#select-editPersonnelFacility').val() != '') {
		            var facilityId = $scope.facility[$('#select-editPersonnelFacility').val()].FacilityId
		        }

            
		        $scope.users[$scope.editUserValue].FirstName = firstName;
		        $scope.users[$scope.editUserValue].LastName = lastName;
		        $scope.users[$scope.editUserValue].UserName = userName;
		        $scope.users[$scope.editUserValue].EmployeeType = role;
                $scope.users[$scope.editUserValue].FacilityId = facilityId;
		     

		        datacontext.saveChanges(function (returnState) {
		            if (returnState) {
		                $scope.refreshUsers();
		                console("Information updated.");
		            };
		        })
              
		        

		    }



		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // SELECT ROLES 
		    $("#select-personnelRole").select2({
		        placeholder: "Select role",
		        data: $scope.roleData.data,
		    });
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // SELECT SURVEY
		    $('#select-survey').on("change", function () {
		        var indexNum    = $('#select-survey').val();
		        var facilityId  = $scope.survey[indexNum].FacilityId;
		        var surveyId = $scope.survey[indexNum].SurveyId;
		        var surveyName = $scope.survey[indexNum].SurveyName;

		        $scope.surveyname = surveyName;
		        $scope.currentIndexNum = indexNum;
		        $scope.currentSurveyId = surveyId;		        
		        $scope.refreshQuestionsById($scope.currentSurveyId);


		    })
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // SELECT FACILITY
		    $('#select-addFacility').on("change", function () {
		        var indexNum = $('#select-addFacility').val();
		        $scope.facilityIndexNum = indexNum; 
		        $scope.currentFacilityId = $scope.facility[indexNum].FacilityId;
		        $scope.facilityName = $scope.facility[$('#select-addFacility').val()].FacilityName;
		        $scope.refreshSurveyById($scope.currentFacilityId);
		    })
		    //
		    //////////////////////////////////
		  
		    //////////////////////////////////
		    // BROADCASTED FROM LAYOUTCONTROLLER
		    $scope.$on('usersLoaded', function (event) {
		        $scope.userIsLoaded = true; 
		        $("#select-addFacility").val('');

		        $("#select-personnel").select2({
		            placeholder: "Select a personnel member",
		            data: conversionFactory.convertMultiDataArray($scope.users, 'LastName', 'FirstName')
		        });

		        $("#select-personnel").on('change', function () {

		            var indexNum = $(this).attr('indexData');
		            var value = $(this).val();
		            $scope.singleUserLoaded = true; 

		            $scope.editUserValue = value; 
		            $scope.editUserName = $scope.users[value].UserName;
		            $scope.editFirstName = $scope.users[value].FirstName;
		            $scope.editLastName = $scope.users[value].LastName;
		            $scope.editFacilityId = $scope.users[value].FacilityId;
		            $scope.editUserRole = $scope.users[value].EmployeeType;
		            $scope.$apply();

		                // GET DEFAULT FACILITY
		                if ($scope.editFacilityId != null) {
		                    datacontext.fetchFacilityWidthId($scope.editFacilityId, function (returnState, data) {
		                        var editFacilityName = (data[0].FacilityName)
		                        $("#select-editPersonnelFacility").select2({
		                            data: conversionFactory.convertToDataArray($scope.facility, 'FacilityName'),
		                        }).select2('val', [conversionFactory.compareAgainstSingle($scope.facility, 'FacilityName', editFacilityName)]); // GET DEFAULT VALUE  
		                    })
		                }
		                else {
		                    $("#select-editPersonnelFacility").select2({
		                        data: conversionFactory.convertToDataArray($scope.facility, 'FacilityName'),
		                    }).select2('val', 0); // GET DEFAULT VALUE  
		                }

                        // GET DEFAULT ROLE
		                if ($scope.editUserRole.toLowerCase() != 'superadmin') {
		                    $("#select-editPersonnelRole").select2({
		                        placeholder: "Select role",
		                        data: $scope.roleData.data,
		                    }).select2('val', [conversionFactory.compareAgainstSingle($scope.roleData.data, 'text', $scope.users[value].EmployeeType)]); // GET DEFAULT VALUE  
		                    $("#select-editPersonnelRole").attr('disabled', false)
		                }
		                else {
		                    $("#select-editPersonnelRole").select2({
		                        placeholder: "Select role",
		                        data: [ { id: 0, text: 'SuperAdmin' }],
		                    }).select2('val', 0); // GET DEFAULT VALUE  
		                    $("#select-editPersonnelRole").attr('disabled', true)
		                }

	
		        })


		    });
		    
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    //
		    $scope.deleteUser = function () {
		        
		        if ($scope.editUserRole.toLowerCase() != "superadmin") {
		            $('#deleteUserModal').modal('show');
		        } else {
		            alert("Cannot delete superadmin accounts.");
		        }
		        
		    }

		    $scope.deleteUserFinal = function () {

		        $scope.users[$scope.editUserValue].entityAspect.setDeleted();
		        datacontext.saveChanges(function (returnState) {
		            if (returnState) {
		                console("User has been deleted.");
		                setTimeout(function () {
		                    location.reload();
		                }, 500)
		            };

		        })

		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
            // BROADCASTED FROM LAYOUTCONTROLLER
		    $scope.$on('facilityLoaded', function (event) {

		        $scope.facilityIsLoaded = true;
		        $("#select-addFacility").val('');
		        $("#select-addFacility").select2({
		                placeholder: "Select a facility",
		                data: conversionFactory.convertToDataArray($scope.facility, 'FacilityName')
		        });

		        $('#select-personnelFacility').select2({
		            placeholder: "Select a facility",
		            data: conversionFactory.convertToDataArray($scope.facility, 'FacilityName')
		        })

		        $('#select-editPersonnelFacility').select2({
		            placeholder: "Select a facility",
		            data: conversionFactory.convertToDataArray($scope.facility, 'FacilityName')
		        })
		    });
		    $scope.refreshFacility();
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // BROADCASTED FROM LAYOUTCONTROLLER
		    $scope.$on('surveyLoaded', function (event) {

		        $scope.quesitonReady = false
		        $scope.questionActive = false;

		        $("#select-survey").val('');
		        $("#select-survey").select2({
		            placeholder: "Select a facility",
		            data: conversionFactory.convertToDataArray($scope.survey, 'SurveyName'),
		        });

                $scope.surveyActive = true;

		        if ($scope.survey.length > 0) {
		            $scope.surveyReady = true;
		            $scope.warningSelectFacility = '';
		        }
		        else {
		            $scope.surveyReady = false;
		            $scope.warningSelectFacility = "- There are no survey's for this facility."
		        }
		    });
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // BROADCASTED FROM LAYOUTCONTROLLER
		    $scope.$on('questionsLoaded', function (event) {

		        
		        //////////////////////////////////
		        //
		        setTimeout(function () {
		            $(".questionTypes").select2({
		                data: $scope.questionTypeData.data
		            }) // GET DEFAULT VALUE     
		            $(".questionTypes").on('change', function(){
		                var indexNum = $(this).attr('indexData');
		                var value = $(this).val();
		                $scope.questions[indexNum].QuestionType = value;
		                $scope.$apply();
		                $scope.saveQuestionEdits();
		            })

		        }, 0);
		        //
		        //////////////////////////////////

		        $scope.questionActive = true;

		        if ($scope.questions.length > 0) {
		            $scope.quesitonReady = true;
		        }
		        else {
		            $scope.quesitonReady = false;

		        }

		    });
		    //
		    //////////////////////////////////
  
		    //////////////////////////////////
		    // ADD FACILITY
		    $scope.addFacility = function (event) {

		        if (event == undefined || conversionFactory.checkForEnter(event.which)) {              // FOR BLUR EVENT or TAB INPUT
		            var name = $('#idNewFacility').val();
		            if (validateStandards(name)) {
		                $scope.warningNewFacility = '';
		                var values = { FacilityName: name };
		                $scope.addFacilityAndSave(values, function (returnState) {
		                    $('#idNewFacility').val('').attr('placeholder', 'Facility added.');
		                    console("Facility added.");
		                })
		            }
		            else {
		                $scope.warningNewFacility = "- Entry is invalid: cannot contain special characters.";
		                $('#idNewFacility').val('').attr('placeholder', 'Facility name...');
		            }
		        }
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // ADD SURVEY
		    $scope.addSurvey = function (event) {
		        if (event == undefined || conversionFactory.checkForEnter(event.which)) {              // FOR BLUR EVENT or TAB INPUT
		            var name = $('#idAddSurvey').val();
		            if (validateStandards(name)) {
		                $scope.warningNewSurvey = '';
		                var values = { FacilityId: $scope.currentFacilityId, SurveyName: name };
		                $scope.addSurveyAndSave(values, function () {
		                    $('#idAddSurvey').val('').attr('placeholder', 'Survey added.');
		                    console("Survey added.");
		                })

		            }
		            else {
		                $scope.warningNewSurvey = "- Entry is invalid: cannot contain special characters.";
		                $('#idNewFacility').val('').attr('placeholder', 'Facility name...');
		            }
		        }
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // ADD QUESTION
		    $scope.addQuestion = function (event) {

		        if (event == undefined || conversionFactory.checkForKey(event.which)) {              // FOR BLUR EVENT or TAB INPUT
		            var text = $('#idNewQuestion').val();
		            if (validateString(text)) {

		                

		                var values = { SurveyId: $scope.currentSurveyId, QuestionType: $('#newQType').val(), QuestionContent: text };
		                $scope.addQuestionAndSave(values, function () {
		                    console("Question added.");
		                    $scope.refreshQuestionsById($scope.currentSurveyId);
		                    $('#idNewQuestion').val('');
		                })
		            }
		            else {

		            }
		        }
               
		    }
		    //
		    //////////////////////////////////
	  
		    //////////////////////////////////
		    // EDIT SURVEY NAME
		    $scope.editSurveyName = function (event) {
	
		        if (event == undefined || conversionFactory.checkForEnter(event.which)) {              // FOR BLUR EVENT or TAB INPUT
		            if ($scope.surveyname != $scope.survey[$scope.currentIndexNum].SurveyName) {     //CHECK IF NAMES ARE DIFFERENT
		                if (validateStandards($scope.surveyname)) {
		                    $scope.survey[$scope.currentIndexNum].SurveyName = $scope.surveyname;
		                    $scope.$apply();
		                    datacontext.saveChanges(function () {
		                        console("Survey name updated.");                                
		                        $scope.refreshSurveyById($scope.currentFacilityId);
		                    });
		                }
		                else {
		                    $scope.surveyname = $scope.survey[$scope.currentIndexNum].SurveyName;
		                }
		            }
		        }
		    }

		    $scope.editFacilityName = function (event) {
		      
		        if (event == undefined || conversionFactory.checkForEnter(event.which)) {              // FOR BLUR EVENT or TAB INPUT		          
		            
		            if ($scope.facilityName != $scope.facility[$scope.facilityIndexNum].FacilityName) {     //CHECK IF NAMES ARE DIFFERENT		                
		                if (validateStandards($scope.facilityName)) {
		                    $scope.facility[$scope.facilityIndexNum].FacilityName = $scope.facilityName;
		                    $scope.$apply();
		                    datacontext.saveChanges(function () {
		                        console("Facility name updated.");
		                        $scope.refreshFacility();
		                    });
		                }
		                else {
		                    $scope.facilityName = $scope.facility[$scope.facilityIndexNum].FacilityName;
		                }
		            }
		        }
		    }

		    
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // SAVE QUESTIONS EDITS
		    $scope.saveQuestionEdits = function (event) {                               
		        if (event == undefined || conversionFactory.checkForKey(event.which)) { // FOR BLUR EVENT or TAB INPUT
		            datacontext.saveChanges(function (returnState) {
		                if (returnState) {
		                    console("Question updated.");
		                }

		            });
		        }
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // CREATE NEW QUESTION
		    $scope.addQuestionAndSave = function (values, callback) {

		        datacontext.createQuestion(values);
		        datacontext.saveChanges(function () {
		            $scope.refreshFacility();
		            callback();
		        });
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // CREATE FACILITY NAME
		    $scope.addFacilityAndSave = function (values, callback) {

		        datacontext.createFacility(values);
		        datacontext.saveChanges(function (returnState) {
		            $scope.refreshFacility();
		            callback(returnState);
		        });
		    }
		    //
		    //////////////////////////////////

            //////////////////////////////////
		    // CREATE FACILITY NAME
		    $scope.addFacilityAndSave = function (values, callback) {
		       
		        datacontext.createFacility(values);
		        datacontext.saveChanges(function () {
		            $scope.refreshFacility();		            
		            callback();
		        });
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // CREATE FACILITY NAME
		    $scope.addSurveyAndSave = function (values, callback) {

		        datacontext.createSurvey(values);
		        datacontext.saveChanges(function () {
		            $scope.refreshSurveyById($scope.currentFacilityId)
		            callback();
		        });
		    }
		    //
		    //////////////////////////////////
		  
		    //////////////////////////////////
		    // DELETE FACILITY
		    $scope.deleteFacility = function () {
		        $('#deleteFacilityModal').modal('show');
		    }

		    $scope.deleteFacilityFinal = function () {		        		        
		        
		        // NOTE TO SELF - I NOW THIS LOOKS STRANGE.  YOU HAVE TO DELETE THE QUESTIONS WITH THE MATCHING SURVEY NUMBERS FIRST, 
		        // THEN DELETE THE ACTUAL SURVEY, AND FINAL THE FACILITY ID

		        var allSurveyIds = [];
		        
		        // DISABLE BUTTON
		        $('#deleteFacilityBtn').attr('disabled', true)

		        /////////////////
		        // DELETE FACILITY
		        $scope.facility[$scope.facilityIndexNum].entityAspect.setDeleted();
		        datacontext.saveChanges(function (returnState) {
		            if (returnState) {
		                $('#deleteFacilityModal').modal('hide');
		                $scope.refreshFacility();
		                $scope.warningSelectFacility = '';
		                $('#select-addFacility').val('');		               
		                $scope.setDefault();
		            }
		        });
		        ///////////////
	        
		        /////////////////
		        // <-  GET ALL SURVEY NUMS ASSOCIATED WITH FACILITY
		        datacontext.fetchSureyWithId($scope.currentFacilityId, function (returnState, data) {		           
		           
		            for (i = 0; i < data.length; i++) {
		                allSurveyIds.push(data[i].SurveyId);
		            }
                   
		            

                    // STEP 1 -  DELETE ALL QUESTIONS WITH MATCHING SURVEY IDS
		            var qCount = 0;
		            function deleteQuestions(q) {
		                datacontext.fetchQuestionsWithId(allSurveyIds[q], function (returnState, data) {
                            
		                    for (i = 0; i < data.length; i++) {
		                        data[i].entityAspect.setDeleted();
		                    }
                            qCount++;

		                    if (qCount < allSurveyIds.length) {		                      
		                        deleteQuestions(qCount);
		                    }
		                    else {		                       
		                        datacontext.saveChanges(function (returnState) {
		                            deleteNullSurveys();
		                        });
		                    };

		                })


		            }
		           
		          
                    // STEP 2 - DELETE NULLS
		            function deleteNullSurveys() {
                       
		                datacontext.fetchSureyWithId(null, function (returnState, data) {
		                    
		                    for (i = 0; i < data.length; i++) {
		                        data[i].entityAspect.setDeleted();
		                    }
		                    /////////////////
		                    // DELETE SURVEYS
                                 datacontext.saveChanges(function (returnState) {
		                                    console("Facility deleted.");
		                                    // DISABLE BUTTON
		                                    $('#deleteFacilityBtn').attr('disabled', false)
		                         });
		                    });
		                    //
                            /////////////////
		            }

		            if (allSurveyIds.length > 0) {
		                deleteQuestions(qCount);
		            }
		            else {
		                deleteNullSurveys();
		            };
                      
		        });
                ///////////////

		    };
		    //
		    //////////////////////////////////
		   		   
		    //////////////////////////////////
		    // DELETE FACILITY
		    $scope.deleteSurvey = function () {	      
		        $('#deleteSurveyModal').modal('show');
		    };

		    $scope.deleteSurveyFinal = function () {

		        // HIDE QUESTIONS
		        $scope.questionActive = false;


		        /////////////////
		        // DELETE SURVEYS
		        $scope.survey[$scope.currentIndexNum].entityAspect.setDeleted();
		        datacontext.saveChanges(function (returnState) {
		            if (returnState) {

		                $('#deleteSurveyModal').modal('hide');
		                $scope.refreshSurveyById($scope.currentFacilityId);
		                console("Survey deleted.");
		                $('#select-survey').val('');
		                $scope.sterilizeDatabase(function () { });

		                $scope.sterilizeDatabase(function(){
		                    $scope.refreshSurveyById($scope.currentFacilityId);
		                    console("Survey deleted.");
		                    $('#select-survey').val('');
                        })

		            }

		        });
		        /////////////////

		       
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // DELETE QUESTION
		    $scope.deleteQuestion = function (indexNum) {
		        $scope.questions[indexNum].entityAspect.setDeleted();
		        datacontext.saveChanges(function (returnState) {
		            if (returnState) {
		                $scope.refreshQuestionsById($scope.currentSurveyId);
		                console("Question deleted.");		                
		            }

		        });
		    }
		    //
		    //////////////////////////////////

		    //////////////////////////////////
		    // STERALIZE DATABASE
		    $scope.sterilizeDatabase = function (callback) {
		       

                // CLEAN OUT NULL SURVEYS
		        datacontext.fetchSureyWithId(null, function (returnState, data) {
		            
		            for (i = 0; i < data.length; i++) {
		                data[i].entityAspect.setDeleted();
		            };
		            set(function () {


		                // CLEAN OUT NULL QUESTIONS
		                datacontext.fetchQuestionsWithId(null, function (returnState, data) {
		                    for (i = 0; i < data.length; i++) {
		                        data[i].entityAspect.setDeleted();
		                    };

		                    set(function () { });

		                })


		            });
		        });


              
		        
		        
		        function set(callback) {
		            setTimeout(function () {
		                datacontext.saveChanges(function (returnState) {
		                    if (returnState) { 
		                        callback();
		                    }

		                });
		            }, 1000);
		        }

		    }
		    //
		    //////////////////////////////////      
		}
       


	});
}());

