	
(function () {
    myApp.controller('landing2Cntrl', function ($scope) {

        $scope.customCopy = 'You have the ease and ability to customize our electronic surveys directly for your staff and departmental needs.  This allows each specific hospital department to design and investigate unique concerns, and fully understand the interaction of every patient with your staff.';
        $scope.measureCopy = 'Image handling patients our electronic survey during the discharge.  The hands-on experience allows your patients the ability to quick & accurately rate their experience in real-time.  Sampling the entire population of patients will provide confidence in the feedback experience.';
        $scope.reportCopy = 'Our online reporting tools offer both high-level overview information and drilldown to the individual provider or nurse level data.  View trends over time or scan a certain shift to help determine the effective of care teams within your facility.';




    });

}());
