﻿function GetProjectPath {
	param (
	    
	)
	PROCESS {
		foreach ($property in (Get-Project).Properties)		{
			
			if ($property.Name -eq 'LocalPath') {
				return $property.Value
			}
		}
	}
}

function CreateDeployFileContent {
 [CmdletBinding()]
	param (
		[parameter(Position=0)]
        [string]$deployFilePath,
		[parameter(Position=1)]
        [string]$exeLocation
	)
	PROCESS {
		Try {
			$projectName = (Get-Project).Properties.Item('AssemblyName').Value
			$content = "& (Join-Path `"`$scriptPath`" $projectName.exe) -buildversion `"`$OctopusReleaseNumber`" | Write-Host"
			$dstream = [System.IO.StreamWriter] $deployFilePath	
		  	$dstream.WriteLine("`$scriptPath = split-path -parent `$MyInvocation.MyCommand.Definition")
			$dstream.WriteLine($content)
			$dstream.close()
			
		}		
		Catch
		{
			Write-Error $_.Exception.Message
			Write-Host "Failed to add AppConfig!"
		}
	}
}

function CreateDeployFailedFileContent {
 [CmdletBinding()]
	param (
		[parameter(Position=0)]
        [string]$deployFailedFilePath,
		[parameter(Position=1)]
        [string]$exeLocation
	)
	PROCESS {
		Try {
			
			$projectName = (Get-Project).Properties.Item('AssemblyName').Value
			$content = "& (Join-Path `"`$scriptPath`" $projectName.exe) -buildversion `"`$OctopusReleaseNumber`" -rollbacktopreviousbuild true | Write-Host"
			$dstream = [System.IO.StreamWriter] $deployFailedFilePath	
		  	$dstream.WriteLine("`$scriptPath = split-path -parent `$MyInvocation.MyCommand.Definition")
			$dstream.WriteLine($content)
			$dstream.close()			
		}		
		Catch
		{
			Write-Error $_.Exception.Message
			Write-Host "Failed to add AppConfig!"
		}
	}
}

function SetupDeployFile {
 [CmdletBinding()]
	param (
		[parameter(Position=0)]
        [string]$toolsPath	
	)
	PROCESS {
		Try {
			
			$projPath = GetProjectPath
			$depoyFilePath = (Join-Path $projPath "Deploy.ps1")			
			CreateDeployFileContent $depoyFilePath $toolsPath
			(Get-Project).ProjectItems.Item('Deploy.ps1').Properties.Item('CopyToOutputDirectory').Value=1			
		}
		Catch
		{
			Write-Error $_.Exception.Message
			Write-Host "Failed to setup the deploy file!"
		}
	}	
}

function SetupDeployFailedFile {
 [CmdletBinding()]
	param (
		[parameter(Position=0)]
        [string]$toolsPath	
	)
	PROCESS {
		Try {
				
			$projPath = GetProjectPath
			$depoyFailedFilePath = (Join-Path $projPath "DeployFailed.ps1")
			
			CreateDeployFailedFileContent $depoyFailedFilePath $toolsPath

			(Get-Project).ProjectItems.Item('DeployFailed.ps1').Properties.Item('CopyToOutputDirectory').Value=1
									
		}
		Catch
		{
			Write-Error $_.Exception.Message
			Write-Host "Failed to setup the deploy file!"
		}
	}	
}




