﻿function GetProjectLocalPath {
	param (
		 [parameter(Position=0, ValueFromPipelineByPropertyName=$true)]
        $Proj		
	)
	PROCESS {
		foreach ($property in $Proj.Properties)		{
			
			if ($property.Name -eq 'LocalPath') {
				return $property.Value
			}
		}
	}
}

<#Gets the dacpac file path#>
function GetSqlProjectDacPacLocation {
	param (
		 [parameter(Position=0, ValueFromPipelineByPropertyName=$true)]
        $dbupProj
	)
	PROCESS {
		$xml = [xml](Get-Content (Join-Path (GetProjectLocalPath $dbupProj) App.config))
			
		if ($xml -eq $null) {
				throw "Could not find app.config for $dbUpProject"
		}
		Write-Host "Retrieving dacpac location"
		$dacpac = (Select-Xml '/configuration/appSettings/add[@key="dacpacPath"]' $xml |%{$_.Node.Attributes})[1].value
		
		if ($dacpac -eq '') {
				throw "No dacpack path has been defined"
		}
		else {						
			$absoluteDacpac = (Join-Path  (GetProjectLocalPath $dbupProj) $dacpac)			
			return $absoluteDacpac
		}
	}
}


<#Used to get a project in the sln by name#>
function GetProjectByName {
	param (
		 [parameter(Position=0, ValueFromPipelineByPropertyName=$true)]
        [string]$dbUpProject			
	)
	PROCESS {
		Write-Host ("Getting Project for '"  + $dbUpProject + "'")				
		$dbupProj = (Get-Project $dbUpProject)
		
		if ($dbupProj -eq $null) {
			throw ("Could not find the project '" + $dbUpProject + "'")
		}
		return $dbupProj
	}
}

function GetCIServerConnectionString {
	param (
		 [parameter(Position=0, ValueFromPipelineByPropertyName=$true)]
        $dbupProj
	)
	PROCESS {
	
			$xml = [xml](Get-Content (Join-Path (GetProjectLocalPath $dbupProj) App.config))
			
			if ($xml -eq $null) {
				throw "Could not find app.config for $dbUpProject"
			}
		Write-Host "Retrieving CI Server"
		$CIServerConnectionString = (Select-Xml '/configuration/connectionStrings/*[1]' $xml |%{$_.Node.Attributes})[1].value
		
		if ($CIServerConnectionString -eq '') {
				throw "No CI server has been defined"
		}
		else {
			Write-Host "CI Server: $CIServerAttr"
			return $CIServerConnectionString
		}
	}
}



function CreateScript {
	param (
		 [parameter(Position=0, ValueFromPipelineByPropertyName=$true)]
        $dacpacFilePath,
		[parameter(Position=1, ValueFromPipelineByPropertyName=$true)]
        $destinationPath,
		[parameter(Position=2, ValueFromPipelineByPropertyName=$true)]
        $targetConnectionString,
		[parameter(Position=3, ValueFromPipelineByPropertyName=$true)]
		$dbupProj,
		[parameter(Position=4, ValueFromPipelineByPropertyName=$true)]
		$force
	)
	PROCESS {
			$packagerPath = "C:\Program Files (x86)\Microsoft SQL Server\110\DAC\bin\SqlPackage.exe" 
			
			if (!(Test-Path $packagerPath)) {
				throw "$packagerPath does not exist!"			
			}
			
			 $Arguments = @()
 	    	$Arguments += "/Action:Script"
 			$Arguments += "/SourceFile:`"$dacpacFilePath`""
 			$Arguments += "/OutputPath:`"$destinationPath`""
			$Arguments += "/TargetConnectionString:`"$targetConnectionString`""

			if ($force) {
				$Arguments += "/p:TreatVerificationErrorsAsWarnings=True"
			}

			$Arguments += "/p:CommentOutSetVarDeclarations=True"
			
			
 		Write-Host "Starting script generation..............." 
			Write-Host "Running Process: $packagerPath" 
			Write-Host "With arguments: $Arguments" 
 			 
 		$process = (Start-Process $packagerPath -ArgumentList $Arguments -Wait -PassThru)
		$process.WaitForExit()
		$exitcode = $process.ExitCode
		Write-Host "...............completed with exit code $exitcode" 
		
		if (!($exitcode -eq 0)) {
				throw "Failed to create update script - sqlpackager.exe exited with exit code : $exitcode"
			}
			
		$newItem = ($dbupProj).ProjectItems.Item("scripts").ProjectItems.AddFromFile($destinationPath)		
		
		$newItem.Properties.Item("ItemType").Value = "EmbeddedResource"
			
	}
}

function ExtractTargetDatabase {
	param (
		[parameter(Position=1, ValueFromPipelineByPropertyName=$true)]
        $targetDacPac,
		[parameter(Position=2, ValueFromPipelineByPropertyName=$true)]
        $targetConnectionString
	)
	PROCESS {
			$packagerPath = "C:\Program Files (x86)\Microsoft SQL Server\110\DAC\bin\SqlPackage.exe" 
			
			if (!(Test-Path $packagerPath)) {
				throw "$packagerPath does not exist!"			
			}
			
			 $Arguments = @()
 	    	$Arguments += "/Action:Extract"
 			$Arguments += "/tf:`"$targetDacPac`"" 			
			$Arguments += "/scs:`"$targetConnectionString`""

			
 		Write-Host "Extracting schema information..............." 
			Write-Host "Running Process: $packagerPath" 
			Write-Host "With arguments: $Arguments" 
 			 
 		
		$process = (Start-Process $packagerPath -ArgumentList $Arguments -Wait -PassThru)
		$process.WaitForExit()
		$exitcode = $process.ExitCode
		Write-Host "...............completed with exit code $exitcode" 
		
		if (!($exitcode -eq 0)) {
				throw "Failed to extract update script - sqlpackager.exe exited with exit code : $exitcode"
		}	
	}
}

function CreateUndoScript {
	param (
		 [parameter(Position=0, ValueFromPipelineByPropertyName=$true)]
        $dacpacFilePath,
		[parameter(Position=1, ValueFromPipelineByPropertyName=$true)]
        $destinationPath,
		[parameter(Position=2, ValueFromPipelineByPropertyName=$true)]
        $targetConnectionString,
		[parameter(Position=3, ValueFromPipelineByPropertyName=$true)]
		$dbupProj,
		[parameter(Position=4, ValueFromPipelineByPropertyName=$true)]
		$force
		
		
	)
	PROCESS {
	
			$databaseName = $targetConnectionString.Substring($targetConnectionString.IndexOf("Initial Catalog") + 16)
			$databaseName = $databaseName.Substring(0, $databaseName.IndexOf(";"))
			$targetDacPac = ($dacpacFilePath + ".target.dacpac") 

			ExtractTargetDatabase $targetDacPac $targetConnectionString
			
			$packagerPath = "C:\Program Files (x86)\Microsoft SQL Server\110\DAC\bin\SqlPackage.exe" 
			
			if (!(Test-Path $packagerPath)) {
				throw "$packagerPath does not exist!"			
			}
			
			 $Arguments = @()
 	    	$Arguments += "/Action:Script"
 			$Arguments += "/tf:`"$dacpacFilePath`""
 			$Arguments += "/OutputPath:`"$destinationPath`""
			$Arguments += "/sf:`"$targetDacPac`""
			$Arguments += "/tdn:`"$databaseName`""
			$Arguments += "/p:DropObjectsNotInSource=True"
			$Arguments += "/p:BlockOnPossibleDataLoss=False"
			$Arguments += "/p:CommentOutSetVarDeclarations=True"
			$Arguments += "/p:AllowIncompatiblePlatform=True"
			if ($force) {
				$Arguments += "/p:TreatVerificationErrorsAsWarnings=True"
			}

			
 		Write-Host "Starting script generation..............." 
			Write-Host "Running Process: $packagerPath" 
			Write-Host "With arguments: $Arguments" 
 			 
 		
		$process = (Start-Process $packagerPath -ArgumentList $Arguments -Wait -PassThru)
		$process.WaitForExit()
		$exitcode = $process.ExitCode
		Write-Host "...............completed with exit code $exitcode" 
		
		if (!($exitcode -eq 0)) {
				throw "Failed to create update script - sqlpackager.exe exited with exit code : $exitcode"
			}
			
		$newItem = ($dbupProj).ProjectItems.Item("scripts").ProjectItems.AddFromFile($destinationPath)		
		
		$newItem.Properties.Item("ItemType").Value = "EmbeddedResource"
			
	}
}

function AddSqlMigration {
 [CmdletBinding()]
	param (
		[string]$toolsPath, 
		[bool]$force = $false		
	)
	PROCESS {
		Try {
			Write-Host "Starting..."
			
			$dbupProj = Get-Project
			
			$dacpac = GetSqlProjectDacPacLocation $dbupProj
								
			Write-Host "Retrieving CI connection string"			
			$CIServerConnectionString = GetCIServerConnectionString $dbupProj
			$newFileName = (Get-Date).Ticks
			$scriptsFolder = "Scripts\$newFileName.sql"
			$scriptsUndoFolder = "Scripts\$newFileName" + "_Undo.sql"
			$desitnationFile = (Join-Path (GetProjectLocalPath $dbupProj) $scriptsFolder)
			$desitnationUndoFile = (Join-Path (GetProjectLocalPath $dbupProj) $scriptsUndoFolder)
			Write-Host "Generating upgrade script: $desitnationFile"
			CreateScript $dacpac $desitnationFile $CIServerConnectionString $dbupProj $force
			Write-Host "Generating undo script: $desitnationUndoFile"
			CreateUndoScript $dacpac $desitnationUndoFile $CIServerConnectionString $dbupProj $force
			
			Write-Host "Migration Script Created Successfully!"
		}
		Catch
		{
			Write-Error $_.Exception.Message
			Write-Host "Migration Script Failed!"
		}
	}	
}


Register-TabExpansion 'AddSqlMigration' @{
	
}

Export-ModuleMember AddSqlMigration